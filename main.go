package main

import (
	metal "github.com/pulumi/pulumi-equinix-metal/sdk/v2/go/equinix"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		// Create an Equinix Metal resource (Project)
		project, err := metal.NewProject(ctx, "pulumi-yetiops", &metal.ProjectArgs{
			Name: pulumi.String("pulumi-yetiops"),
			BgpConfig: metal.ProjectBgpConfigArgs{
				Asn:            pulumi.Int(65000),
				DeploymentType: pulumi.String("local"),
			},
		})
		if err != nil {
			return err
		}

		// Export the name of the project
		ctx.Export("projectName", project.Name)
		ctx.Export("projectId", project.ID())
		return nil
	})
}
