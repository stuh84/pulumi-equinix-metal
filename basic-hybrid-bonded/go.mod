module basic-hybrid-bonded

go 1.14

require (
	github.com/pulumi/pulumi-equinix-metal/sdk/v2 v2.0.0
	github.com/pulumi/pulumi/sdk/v3 v3.0.0
)
