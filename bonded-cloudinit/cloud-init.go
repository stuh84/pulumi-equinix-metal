package main

import (
	"fmt"

	"github.com/juju/juju/cloudconfig/cloudinit"
)

type instanceDetails struct {
	IpAddress      string
	GatewayAddress string
	VlanId         int
}

func hybridCloudInitConfig(config *instanceDetails) string {
	c, err := cloudinit.New("focal")

	if err != nil {
		panic(err)
	}

	c.SetSystemUpdate(true)

	c.AddPackage("vlan")

	c.AddRunCmd("echo \"8021q\" >> /etc/modules")

	c.AddRunCmd("modprobe 8021q")

	vlanConfig := fmt.Sprintf(
		"auto bond0.%v\niface bond0.%v inet static\n  address %v",
		config.VlanId,
		config.VlanId,
		config.IpAddress,
	)

	c.AddRunTextFile(
		"/etc/network/interfaces.d/vlans",
		vlanConfig,
		0755,
	)

	c.AddRunCmd("echo \"source /etc/network/interfaces.d/*\" >> /etc/network/interfaces")

	c.AddRunCmd("echo \"net.ipv4.ip_forward=1\" > /etc/sysctl.d/ipv4forward.conf; sysctl --system")

	c.AddRunCmd(
		fmt.Sprintf(
			"ifup bond0.%v",
			config.VlanId,
		),
	)

	c.AddRunCmd("iptables -t nat -A POSTROUTING -s 0.0.0.0/0 -o bond0 -j MASQUERADE")

	script, err := c.RenderScript()

	if err != nil {
		panic(err)
	}

	return script
}

func bondedCloudInitConfig(config *instanceDetails) string {
	c, err := cloudinit.New("focal")

	if err != nil {
		panic(err)
	}

	c.AddRunCmd("echo \"8021q\" >> /etc/modules")

	c.AddRunCmd("modprobe 8021q")

	vlanConfig := fmt.Sprintf(
		"auto bond0.%v\niface bond0.%v inet static\n  address %v\n  gateway %v\n",
		config.VlanId,
		config.VlanId,
		config.IpAddress,
		config.GatewayAddress,
	)

	c.AddRunTextFile(
		"/etc/network/interfaces.d/vlans",
		vlanConfig,
		0755,
	)

	c.AddRunCmd(
		fmt.Sprintf(
			"ip link add link bond0 name bond0.%v type vlan id %v",
			config.VlanId,
			config.VlanId,
		),
	)

	c.AddRunCmd(
		fmt.Sprintf(
			"ip addr add %v dev bond0.%v; ip link set bond0.%v up",
			config.IpAddress,
			config.VlanId,
			config.VlanId,
		),
	)

	c.AddRunCmd(
		fmt.Sprintf(
			"ip route del 0.0.0.0/0; ip route add 0.0.0.0/0 dev bond0.%v via %v",
			config.VlanId,
			config.GatewayAddress,
		),
	)

	c.AddRunCmd("apt-get update && apt-get install -y vlan")

	c.AddRunCmd("echo \"source /etc/network/interfaces.d/*\" >> /etc/network/interfaces")

	c.AddRunCmd("sed -i '/gateway/d' /etc/network/interfaces")

	script, err := c.RenderScript()

	if err != nil {
		panic(err)
	}

	return script
}
