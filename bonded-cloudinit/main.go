package main

import (
	"fmt"
	"io/ioutil"
	"os/user"

	metal "github.com/pulumi/pulumi-equinix-metal/sdk/v2/go/equinix"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")

		rootStack, err := pulumi.NewStackReference(ctx, "yetiops/equinix-metal-yetiops/staging", nil)
		if err != nil {
			return err
		}

		projectId := rootStack.GetStringOutput(pulumi.String("projectId"))

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)

		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := metal.NewProjectSshKey(ctx, commonName, &metal.ProjectSshKeyArgs{
			Name:      pulumi.String(commonName),
			PublicKey: pulumi.String(sshkey_contents),
			ProjectId: projectId,
		})

		if err != nil {
			return err
		}

		hybridVlan, err := metal.NewVlan(ctx, "hybrid-vlan", &metal.VlanArgs{
			Description: pulumi.String("VLAN between hybrid-01 and bonded-01"),
			ProjectId:   projectId,
			Metro:       pulumi.String("am"),
		})

		if err != nil {
			return err
		}

		hybridACloudConfig := pulumi.All(
			hybridVlan.Vxlan,
		).ApplyT(
			func(args []interface{}) string {
				hybridADetails := instanceDetails{
					IpAddress:      "172.20.40.1/24",
					VlanId:         args[0].(int),
					GatewayAddress: "",
				}
				return hybridCloudInitConfig(&hybridADetails)
			},
		)

		hybridA, err := metal.NewDevice(ctx, "hybrid-01", &metal.DeviceArgs{
			Hostname:        pulumi.String("hybrid-01"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("debian_10"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.StringOutput(pulumi.Sprintf("%v", hybridACloudConfig)),
		})

		if err != nil {
			return err
		}

		hybridANetwork, err := metal.NewDeviceNetworkType(ctx, "hybrid-01-hybridnetwork", &metal.DeviceNetworkTypeArgs{
			DeviceId: hybridA.ID(),
			Type:     pulumi.String("hybrid-bonded"),
		})

		if err != nil {
			return err
		}

		bondedACloudConfig := pulumi.All(
			hybridVlan.Vxlan,
		).ApplyT(
			func(args []interface{}) string {
				bondedADetails := instanceDetails{
					IpAddress:      "172.20.40.2/24",
					VlanId:         args[0].(int),
					GatewayAddress: "172.20.40.1",
				}
				return bondedCloudInitConfig(&bondedADetails)
			},
		)
		bondedA, err := metal.NewDevice(ctx, "bonded-01", &metal.DeviceArgs{
			Hostname:        pulumi.String("bonded-01"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("debian_10"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.StringOutput(pulumi.Sprintf("%v", bondedACloudConfig)),
		}, pulumi.DependsOn(
			[]pulumi.Resource{
				hybridA,
			},
		))

		if err != nil {
			return err
		}

		bondedANetwork, err := metal.NewDeviceNetworkType(ctx, "bonded-01-network", &metal.DeviceNetworkTypeArgs{
			DeviceId: bondedA.ID(),
			Type:     pulumi.String("layer2-bonded"),
		})

		if err != nil {
			return err
		}

		_, err = metal.NewPortVlanAttachment(ctx, "hybrid-01-vlan-attach", &metal.PortVlanAttachmentArgs{
			DeviceId: hybridANetwork.ID(),
			PortName: pulumi.String("bond0"),
			VlanVnid: hybridVlan.Vxlan,
		})

		if err != nil {
			return err
		}

		_, err = metal.NewPortVlanAttachment(ctx, "bonded-01-vlan-attach", &metal.PortVlanAttachmentArgs{
			DeviceId: bondedANetwork.ID(),
			PortName: pulumi.String("bond0"),
			VlanVnid: hybridVlan.Vxlan,
		})

		if err != nil {
			return err
		}

		ctx.Export("hybridAIP", hybridA.AccessPublicIpv4)
		ctx.Export("bondedAIP", bondedA.AccessPublicIpv4)
		ctx.Export("hybridVlanId", hybridVlan.Vxlan)
		return nil
	})
}
