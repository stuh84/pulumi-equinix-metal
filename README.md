# Pulumi with Equinix Metal

This repository contains examples of using Pulumi with Equinix Metal that takes advantage of the different [Layer 2 Network Types](https://metal.equinix.com/developers/docs/layer2-networking/overview/), as well as BGP where applicable.

## Examples

Examples available are: -

* [basic-hybrid-bonded](basic-hybrid-bonded/) - Two c3.small instances, using [Hybrid Bonded mode](https://metal.equinix.com/developers/docs/layer2-networking/hybrid-bonded-mode/), no UserData
* [hybrid-bonded-cloudinit](hybrid-bonded-cloudinit/) - Two c3.small instances, using [Hybrid Bonded mode](https://metal.equinix.com/developers/docs/layer2-networking/hybrid-bonded-mode/), UserData to add the VLAN interface between the two
* [basic-bonded](basic-bonded/) - Two c3.small instances, one using [Hybrid Bonded mode](https://metal.equinix.com/developers/docs/layer2-networking/hybrid-bonded-mode/), the other using [Layer 2 Bonded mode](https://metal.equinix.com/developers/docs/layer2-networking/layer2-mode/#converting-to-layer-2-bonded-mode), no UserData
* [bonded-cloudinit](bonded-cloudinit/) - Two c3.small instances, one using [Hybrid Bonded mode](https://metal.equinix.com/developers/docs/layer2-networking/hybrid-bonded-mode/), the other using [Layer 2 Bonded mode](https://metal.equinix.com/developers/docs/layer2-networking/layer2-mode/#converting-to-layer-2-bonded-mode), UserData to add the VLAN interface, and make the Hybrid Bonded the default gateway for the Layer 2 Bonded server 
* [anycast-bgp](anycast-bgp/) - Two c3.small instances with BGP sessions, a single c3.small Salt Master, installing ExaBGP to advertise an anycast IP (which can be verified with NGINX running on both)
  * Salt states for this are available [here](https://gitlab.com/stuh84/salt-anycast-equinix)
