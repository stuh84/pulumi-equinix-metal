package main

import (
	"fmt"

	"github.com/juju/juju/cloudconfig/cloudinit"
)

type instanceDetails struct {
	IpAddress string
	VlanId    int
}

func cloudInitConfig(config *instanceDetails) string {
	c, err := cloudinit.New("focal")

	if err != nil {
		panic(err)
	}

	c.SetSystemUpdate(true)

	c.AddPackage("vlan")

	c.AddRunCmd("echo \"8021q\" >> /etc/modules")

	c.AddRunCmd("modprobe 8021q")

	vlanConfig := fmt.Sprintf(
		"auto bond0.%v\niface bond0.%v inet static\n  address %v",
		config.VlanId,
		config.VlanId,
		config.IpAddress,
	)

	c.AddRunTextFile(
		"/etc/network/interfaces.d/vlans",
		vlanConfig,
		0755,
	)

	c.AddRunCmd("echo \"source /etc/network/interfaces.d/*\" >> /etc/network/interfaces")

	c.AddRunCmd(
		fmt.Sprintf(
			"ifup bond0.%v",
			config.VlanId,
		),
	)

	script, err := c.RenderScript()

	if err != nil {
		panic(err)
	}

	return script
}
