package main

import (
	"fmt"
	"io/ioutil"
	"os/user"

	metal "github.com/pulumi/pulumi-equinix-metal/sdk/v2/go/equinix"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")

		rootStack, err := pulumi.NewStackReference(ctx, "yetiops/equinix-metal-yetiops/staging", nil)
		if err != nil {
			return err
		}

		projectId := rootStack.GetStringOutput(pulumi.String("projectId"))

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)

		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := metal.NewProjectSshKey(ctx, commonName, &metal.ProjectSshKeyArgs{
			Name:      pulumi.String(commonName),
			PublicKey: pulumi.String(sshkey_contents),
			ProjectId: projectId,
		})

		if err != nil {
			return err
		}

		hybridVlan, err := metal.NewVlan(ctx, "hybrid-vlan", &metal.VlanArgs{
			Description: pulumi.String("VLAN between hybrid-01 and hybrid-02"),
			ProjectId:   projectId,
			Metro:       pulumi.String("am"),
		})

		if err != nil {
			return err
		}

		hybridACloudConfig := pulumi.All(
			hybridVlan.Vxlan,
		).ApplyT(
			func(args []interface{}) string {
				hybridADetails := instanceDetails{
					IpAddress: "172.20.40.1/24",
					VlanId:    args[0].(int),
				}
				return cloudInitConfig(&hybridADetails)
			},
		)

		hybridA, err := metal.NewDevice(ctx, "hybrid-01", &metal.DeviceArgs{
			Hostname:        pulumi.String("hybrid-01"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("debian_10"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.StringOutput(pulumi.Sprintf("%v", hybridACloudConfig)),
		})

		if err != nil {
			return err
		}

		hybridANetwork, err := metal.NewDeviceNetworkType(ctx, "hybrid-01-hybridnetwork", &metal.DeviceNetworkTypeArgs{
			DeviceId: hybridA.ID(),
			Type:     pulumi.String("hybrid-bonded"),
		})

		if err != nil {
			return err
		}

		hybridBCloudConfig := pulumi.All(
			hybridVlan.Vxlan,
		).ApplyT(
			func(args []interface{}) string {
				hybridADetails := instanceDetails{
					IpAddress: "172.20.40.2/24",
					VlanId:    args[0].(int),
				}
				return cloudInitConfig(&hybridADetails)
			},
		)

		hybridB, err := metal.NewDevice(ctx, "hybrid-02", &metal.DeviceArgs{
			Hostname:        pulumi.String("hybrid-02"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("debian_10"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.StringOutput(pulumi.Sprintf("%v", hybridBCloudConfig)),
		})

		if err != nil {
			return err
		}

		hybridBNetwork, err := metal.NewDeviceNetworkType(ctx, "hybrid-02-hybridnetwork", &metal.DeviceNetworkTypeArgs{
			DeviceId: hybridB.ID(),
			Type:     pulumi.String("hybrid-bonded"),
		})

		if err != nil {
			return err
		}

		_, err = metal.NewPortVlanAttachment(ctx, "hybrid-01-vlan-attach", &metal.PortVlanAttachmentArgs{
			DeviceId: hybridANetwork.ID(),
			PortName: pulumi.String("bond0"),
			VlanVnid: hybridVlan.Vxlan,
		})

		if err != nil {
			return err
		}

		_, err = metal.NewPortVlanAttachment(ctx, "hybrid-02-vlan-attach", &metal.PortVlanAttachmentArgs{
			DeviceId: hybridBNetwork.ID(),
			PortName: pulumi.String("bond0"),
			VlanVnid: hybridVlan.Vxlan,
		})

		if err != nil {
			return err
		}

		ctx.Export("hybridAIP", hybridA.AccessPublicIpv4)
		ctx.Export("hybridBIP", hybridB.AccessPublicIpv4)
		ctx.Export("hybridVlanId", hybridVlan.Vxlan)
		return nil
	})
}
